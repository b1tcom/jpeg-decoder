#include "huffman.h"
#include <cstdint>
#include <cstdio>
#include <stdexcept>

HuffmanTree::HuffmanTree() {
    root_ = new Node();
    root_->CreateChildren();
}

HuffmanTree::HuffmanTree(HuffmanTree&& tree) {
    std::swap(root_, tree.root_);
}

HuffmanTree::HuffmanTree(const HuffmanTree& tree) {
    root_ = tree.root_->Duplicate();
    current_ = tree.current_;
}

HuffmanTree& HuffmanTree::operator=(HuffmanTree&& tree) {
    if (this != &tree) {
        std::swap(root_, tree.root_);
    }
    return *this;
}

HuffmanTree& HuffmanTree::operator=(const HuffmanTree& tree) {
    HuffmanTree new_tree{tree};
    *this = std::move(new_tree);
    return *this;
}

void HuffmanTree::BuildTree(const std::vector<uint8_t>& frequencies, const std::vector<uint8_t>& values) {
    auto value_iter = values.begin();
    std::queue<Node*> queue;
    queue.push(root_->next[0]);
    queue.push(root_->next[1]);
    size_t num_of_current_level_nodes = queue.size();
    for (uint8_t i : frequencies) {
        for (size_t j = 0; j < i; ++j) {
            Node* front = queue.front();
            queue.pop();
            --num_of_current_level_nodes;
            front->MakeLeaf(*value_iter++);
        }

        for (size_t j = 0; j < num_of_current_level_nodes; ++j) {
            Node* front = queue.front();
            queue.pop();
            front->CreateChildren();
            queue.push(front->next[0]);
            queue.push(front->next[1]);
        }
        num_of_current_level_nodes = queue.size();
    }
}

void HuffmanTree::printAllLeaves() const {
    std::printf("Huffman tree traverse\n");
    printAllLeaves(root_, "");
}

void HuffmanTree::StartMoving() {
    current_ = root_;
}

bool HuffmanTree::Move(uint8_t bit) {
    if (!current_->next[bit])
        throw std::runtime_error("The node, you're trying to move to, doesn't exist");
    current_ = current_->next[bit];
    return current_->leaf_value.has_value();
}

uint8_t HuffmanTree::GetLeafValue() const {
    if (!current_ || !current_->leaf_value.has_value())
        throw std::runtime_error("The current node isn't a leaf");
    return current_->leaf_value.value();
}

void HuffmanTree::printAllLeaves(HuffmanTree::Node* node, std::string path) const {
    if (!node)
        return;
    if (node->leaf_value) {
        std::printf("path=%s, value=%.2X\n", path.c_str(), node->leaf_value);
    }
    printAllLeaves(node->next[0], path + '0');
    printAllLeaves(node->next[1], path + '1');
}

HuffmanTree::~HuffmanTree() {
    if (root_) delete root_;
}
