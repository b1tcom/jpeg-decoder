#include <fstream>
#include <iostream>

#include "decoder.h"
#include "png_encoder.h"

int main() {
    std::string image_path = "../tests/chroma_halfed.jpg";
    Image image = Decode(image_path);
    WritePng("wufwuf.png", image);
}