#include "jpeg_decoder.h"

#include <exception>
#include <iostream>
#include <sstream>
#include <string>
#include <utility>

#include "dct.h"
#include "exceptions.h"

const bool kDebug = false;
//const bool kDebug = true;


int JpegDecoder::Mcu::width = 1, JpegDecoder::Mcu::height = 1;
int JpegDecoder::Mcu::rows = 1, JpegDecoder::Mcu::columns = 1;
uint8_t JpegDecoder::Mcu::max_horiz_sampling = 1;
uint8_t JpegDecoder::Mcu::max_vert_sampling = 1;

JpegDecoder::JpegDecoder(std::istream& in)
    : in_(in), bit_reader_(in) {}

void JpegDecoder::Decode() {
    while (!end_reached_) {
        Match(kMarker);
        DispatchSection();
    }
}

void JpegDecoder::DispatchSection() {
    auto symbol = ReadByte();
    if (0xe0 <= symbol && symbol <= 0xef && symbol != kApp1Marker) {
        SkipSection();
        return;
    }
    auto handler_iter = kSectionDecoders.find(symbol);
    if (handler_iter == kSectionDecoders.end()) {
        std::cerr << "Undefined Marker: " << std::hex << std::uppercase << int(symbol) << std::endl;
        throw UnknownSectionMarker();
    }
    handler_iter->second(*this);
}

void JpegDecoder::SoiDecoder() {}

void JpegDecoder::EoiDecoder() {
    if (kDebug) {
        std::printf("Eoi section start\n");
    }

    end_reached_ = true;

    if (kDebug) {
        std::printf("Eoi section end\n");
    }
}

void JpegDecoder::SkipSection() {
    while (!IsNextSectionMarker()) {
        ReadByte();
    }
}

void JpegDecoder::App0Decoder() {
    if (kDebug) {
        printf("APP0 section start\n");
    }
    uint16_t length = ReadSectionLength();
    length -= 2;
    while (length--) {
        ReadByte();
    }

    if (kDebug) {
        printf("APP0 section end\n");
    }
}

void JpegDecoder::App1Decoder() {
    if (kDebug) {
        printf("APP1 section start\n");
    }

    uint16_t length = ReadSectionLength();
    length -= 2;
    while (length--) {
        ReadByte();
    }

    if (kDebug) {
        printf("APP1 section end\n");
    }
}

void JpegDecoder::ComDecoder() {
    if (kDebug) {
        std::printf("Comment section Started\n");
    }
    ReadSectionLength();
    while (!IsNextSectionMarker()) {
        comment_ += ReadByte();
    }
    image_.SetComment(comment_);
    if (kDebug) {
        std::printf("Comment section end\n");
    }
}

void JpegDecoder::DqtDecoder() {
    if (kDebug) {
        std::printf("Dqt Section Started\n");
    }
    uint16_t length = ReadSectionLength();
    if ((length - 2) % 65) {
        throw std::runtime_error("Expected dqt::length == 66 * k");
    }
    for (size_t i = 0, count = length / 66; i < count; ++i) {
        uint8_t pair = ReadByte();
        uint8_t table_id = pair % 16;
        // uint8_t value_length = (pair >= 16);
        const size_t kTableSize = 8;
        std::vector<uint8_t> plain_table;
        for (size_t i = 0; i < kTableSize * kTableSize; ++i)
            plain_table.push_back(ReadByte());
        if (kDebug) {
            int count = 0;
            std::printf("Plain quant_table:\n");
            for (auto i : plain_table) {
                std::printf("%*d ", 3, int(i));
                ++count;
                count %= 8;
                if (!count)
                    std::printf("\n");
            }
            std::printf("\n");
        }
        if (plain_table.size() != kTableSize * kTableSize) {
            throw std::runtime_error("Invalid quantization table size");
        }
        quantization_tables[table_id] = OrderZigzagTable(plain_table);
        if (kDebug) {
            std::printf("Quant table #%d\n", int(table_id));
            for (auto i : quantization_tables[table_id]) {
                for (auto j : i) {
                    std::printf("%d ", int(j));
                }
                std::printf("\n");
            }
        }
    }
    if (kDebug) {
        std::printf("Dqt Section ended\n");
    }
}

template <class T>
std::vector<std::vector<T>>
JpegDecoder::OrderZigzagTable(std::vector<T> plain_table) {
    const size_t kTableSize = 8;
    std::vector<std::vector<T>> table(kTableSize, std::vector<T>(kTableSize));
    auto plain_iter = plain_table.begin();
    for (size_t i = 0; i < kTableSize; ++i) {
        size_t row = i, col = 0;
        int delta_row_direction = (i & 1) ? 1 : -1;
        if (i & 1)
            std::swap(row, col);
        for (size_t j = 0; j <= i; ++j) {
            table[row + delta_row_direction * j][col - delta_row_direction * j] = *plain_iter++;
        }
    }
    for (size_t i = 1; i < kTableSize; ++i) {
        size_t row = i, col = kTableSize - 1;
        int delta_row_direction = (i & 1) ? -1 : 1;
        if (i & 1)
            std::swap(row, col);
        for (size_t j = 0; j < kTableSize - i; ++j) {
            table[row + delta_row_direction * j][col - delta_row_direction * j] = *plain_iter++;
        }
    }
    return table;
}

void JpegDecoder::Sof0Decoder() {
    if (kDebug) {
        std::printf("Sof0 Section started\n");
    }
    ReadSectionLength();
    ReadByte();  // precision

    image_height_ = ReadSectionLength();
    image_width_ = ReadSectionLength();
    image_.SetSize(image_width_, image_height_);

    num_of_components_ = ReadByte();
    for (size_t i = 0; i < num_of_components_; ++i) {
        auto description = ReadSamplingDescription();
        sampling_descriptions_[description.component_id] = description;
    }
    if (kDebug) {
        std::printf("Sof0 Section ended\n");
    }
}

JpegDecoder::SamplingDescription
JpegDecoder::ReadSamplingDescription() {
    SamplingDescription description;
    description.component_id = ReadByte();
    uint8_t sampling = ReadByte();
    description.vertical_sampling = sampling & 0b1111;
    description.horizontal_sampling = sampling >> 4;
    description.table_id = ReadByte();
    return description;
}

void JpegDecoder::DhtDecoder() {
    if (kDebug) {
        std::printf("Dht section started\n");
    }

    ReadSectionLength();
    while (!IsNextSectionMarker()) {
        uint8_t ht_info = ReadByte();
        uint8_t id = ht_info & 0b1111;
        if (id > 3)
            throw std::runtime_error("Huffman table id must be 0..3");
        HuffmanTable::TableType type = ((ht_info >> 4) & 1) ? HuffmanTable::AC : HuffmanTable::DC;
        HuffmanTable& huffman_table = huffman_tables_[{id, type}];
        huffman_table.id = id;
        huffman_table.type = type;
        if (ht_info >> 5)
            throw std::runtime_error("5-7 bits of Huffman table info must be 0");
        size_t total_symbols_num = 0;
        for (size_t i = 0; i < 16; ++i) {
            huffman_table.num_of_symbols.push_back(ReadByte());
            total_symbols_num += huffman_table.num_of_symbols.back();
        }
        for (size_t i = 0; i < total_symbols_num; ++i) {
            huffman_table.symbols.push_back(ReadByte());
        }

        huffman_table.tree.BuildTree(huffman_table.num_of_symbols, huffman_table.symbols);

        if (kDebug) {
            huffman_table.tree.printAllLeaves();
        }
    }

    if (kDebug) {
        std::printf("Dht section ended\n");
    }
}

void JpegDecoder::SosDecoder() {
    if (kDebug) {
        std::printf("Sos section started\n");
    }

    ReadSectionLength();
    uint8_t num_of_components = ReadByte();
    // 1 <= num_of_components <= 4

    for (size_t i = 0; i < num_of_components; ++i) {
        uint8_t comp_id = ReadByte();
        uint8_t tables_id = ReadByte();
        uint8_t ac_table = tables_id & 0b1111;
        uint8_t dc_table = tables_id >> 4;
        components_info_[comp_id] = ComponentInfo{ac_table, dc_table};
    }

    //skip 3 bytes
    for (size_t i = 0; i < 3; ++i)
        ReadByte();

    Mcu::max_horiz_sampling = 1;
    Mcu::max_vert_sampling = 1;
    for (auto[_, description] : sampling_descriptions_) {
        Mcu::max_horiz_sampling = std::max(description.horizontal_sampling, Mcu::max_horiz_sampling);
        Mcu::max_vert_sampling = std::max(description.vertical_sampling, Mcu::max_vert_sampling);
    }

    Mcu::height = 8 * Mcu::max_vert_sampling;
    Mcu::width = 8 * Mcu::max_horiz_sampling;

    Mcu::rows = image_height_ / Mcu::height;
    Mcu::columns = image_width_ / Mcu::width;
    if (image_height_ % Mcu::height)
        ++Mcu::rows;
    if (image_width_ % Mcu::width)
        ++Mcu::columns;

    Grid<Mcu> all_mcu(Mcu::rows, std::vector<Mcu>(Mcu::columns));
    for (size_t i = 0; i < all_mcu.size(); ++i) {
        for (size_t j = 0; j < all_mcu[0].size(); ++j) {
            if (kDebug) {
                std::printf("\nMCU (%d, %d) decoding...\n", i, j);
            }
            all_mcu[i][j] = DecodeMcu();
        }
    }

    DecodeImage(all_mcu);

    if (kDebug) {
        std::printf("Sos section ended\n");
    }
}

void JpegDecoder::QuantizeTable(CanonTable& table, const Table& quant_table) {
    for (size_t i = 0; i < table.size(); ++i) {
        for (size_t j = 0; j < table[0].size(); ++j) {
            table[i][j] *= quant_table[i][j];
        }
    }
}

void JpegDecoder::DecodeImage(Grid<Mcu> all_mcu) {
    for (size_t i = 0; i < all_mcu.size(); ++i) {
        for (size_t j = 0; j < all_mcu[0].size(); ++j) {
            ProcessMcu(all_mcu[i][j], i, j);
        }
    }
}

int JpegDecoder::GetComponentValue(Mcu& mcu, size_t component, size_t mcu_y, size_t mcu_x) {
    int grid_y = (sampling_descriptions_[component].vertical_sampling == 2 && mcu_y >= 8) ? 1 : 0;
    int grid_x = (sampling_descriptions_[component].horizontal_sampling == 2 && mcu_x >= 8) ? 1 : 0;
    int y, x;
    if (Mcu::max_vert_sampling == 1)
        y = mcu_y;
    else if (sampling_descriptions_[component].vertical_sampling == 1)
        y = mcu_y >> 1;
    else
        y = mcu_y % 8;
    if (Mcu::max_horiz_sampling == 1)
        x = mcu_x;
    else if (sampling_descriptions_[component].horizontal_sampling == 1)
        x = mcu_x >> 1;
    else
        x = mcu_x % 8;
    return mcu.tables[component - 1][grid_y][grid_x][y][x];
}

void JpegDecoder::ProcessMcu(Mcu& mcu, size_t row, size_t col) {
    for (size_t component = 1; component <= mcu.tables.size(); ++component) {
        uint8_t table_id = sampling_descriptions_[component].table_id;
        auto& quant_table = quantization_tables[table_id];
        for (auto& row_of_tables : mcu.tables[component - 1]) {
            for (auto& table : row_of_tables) {
                QuantizeTable(table, quant_table);
                ProcessInverseDiscreteCosineTransform(table);
            }
        }
    }

    /* (sampling, grid)
     * (?, 1) => [][][mcu_y][]
     * (1, 2) => [][][mcu_y >> 1][]
     * (2, 2) => [][][mcu_y % 8][]
     */

    std::vector<int> components(num_of_components_);
    for (size_t i = 0; i < Mcu::height; ++i) {
        size_t y = Mcu::height * row + i;
        if (y >= image_height_)
            break;

        for (size_t j = 0; j < Mcu::width; ++j) {
            for (size_t component = 1; component <= num_of_components_; ++component) {
                components[component - 1] = GetComponentValue(mcu, component, i, j);
            }
            size_t x = Mcu::width * col + j;
            if (x >= image_width_) {
                break;
            }
            RGB rgb;
            if (num_of_components_ == 1)
                rgb = YCbCrToRgb(
                    components[0],
                    0,
                    0,
                    true);
            else
                rgb = YCbCrToRgb(
                    components[0],
                    components[1],
                    components[2],
                    false);
            image_.SetPixel(y, x, rgb);
        }
    }
}

JpegDecoder::Mcu JpegDecoder::DecodeMcu() {
    Mcu mcu(num_of_components_);
    for (size_t i = 1; i <= num_of_components_; ++i) {
        mcu.tables[i - 1] = ReadComponentMcuGrid(i);
    }
    return mcu;
}

JpegDecoder::McuGrid
JpegDecoder::ReadComponentMcuGrid(uint8_t component_id) {
    if (kDebug) {
        std::printf("Decoding %d component\n", component_id);
    }
    uint8_t rows = sampling_descriptions_[component_id].vertical_sampling;
    uint8_t columns = sampling_descriptions_[component_id].horizontal_sampling;

    McuGrid tables(rows, std::vector<CanonTable>(columns));
    for (size_t i = 0; i < rows; ++i) {
        for (size_t j = 0; j < columns; ++j) {
            tables[i][j] = OrderZigzagTable(ReadComponentTable(component_id));
        }
    }
    return tables;
}

std::vector<int> JpegDecoder::ReadComponentTable(uint8_t component_id) {
    if (kDebug) {
        std::printf("Table\n");
    }
    std::vector<int> flat_table(64);
    int relative_dc = ReadDc(component_id);
    flat_table[0] = prev_dc_[component_id] + relative_dc;
    prev_dc_[component_id] = flat_table[0];
    if (kDebug) {
        //        std::printf("DC=%d\n", relative_dc);
        std::printf("%d\n", relative_dc);
    }
    uint8_t pos = 1;
    while (pos < flat_table.size()) {
        uint8_t zeros;
        int val;
        std::tie(zeros, val) = ReadAc(component_id);
        if (kDebug) {
            //            std::printf("AC Value=%d\n", val);
            std::printf("%d\n", val);
        }
        pos += zeros;
        if (pos >= flat_table.size())
            break;
        flat_table[pos++] = val;
    }
    return flat_table;
}

int JpegDecoder::ReadDc(uint8_t component_id) {
    HuffmanTableKey key = {components_info_[component_id].dc_table, HuffmanTable::TableType::DC};
    HuffmanTree& tree = huffman_tables_[key].tree;
    tree.StartMoving();
    while (!tree.Move(bit_reader_.Get())) {
    }
    uint8_t pair = tree.GetLeafValue();
    if (!pair)
        return 0;
    if (GetHighWord(pair))
        std::runtime_error("Expected 0 as high word of DC code");
    uint8_t num_of_bits = GetLowWord(pair);
    int value = ReadBitsAsNumber(num_of_bits);
    if (!GetBit(value, num_of_bits - 1))
        value = -InvertFirstBits(value, num_of_bits);
    return value;
}

std::pair<uint8_t, int> JpegDecoder::ReadAc(uint8_t component_id) {
    HuffmanTableKey key = {components_info_[component_id].ac_table, HuffmanTable::TableType::AC};
    HuffmanTree& tree = huffman_tables_[key].tree;
    tree.StartMoving();
    while (!tree.Move(bit_reader_.Get())) {
    }
    uint8_t pair = tree.GetLeafValue();
    if (!pair)  // (0, 0)
        return {64, 0};

    uint8_t zeros = GetHighWord(pair);
    uint8_t num_of_bits = GetLowWord(pair);
    if (!GetLowWord(pair))
        return {zeros, 0};

    int value = ReadBitsAsNumber(num_of_bits);
    if (!GetBit(value, num_of_bits - 1))
        value = -InvertFirstBits(value, num_of_bits);
    return {zeros, value};
}

int JpegDecoder::ReadBitsAsNumber(uint8_t num_of_bits) {
    int res = 0;
    while (num_of_bits--) {
        res = (res << 1) + bit_reader_.Get();
    }
    return res;
}

bool JpegDecoder::IsNextSectionMarker() {
    if (in_.get() != kMarker) {
        in_.unget();
        return false;
    }
    if (in_.get() == kMarker) {
        in_.unget();
        return false;
    }
    in_.unget();
    in_.unget();
    return true;
}

uint16_t JpegDecoder::ReadSectionLength() {
    uint16_t first_bite = ReadByte();
    return (first_bite << 8) + ReadByte();
}

void JpegDecoder::Match(uint8_t symbol) {
    uint8_t bite = ReadByte();
    if (bite != symbol) {
        std::stringstream ss;
        ss << "Expected ";
        ss << std::hex << std::uppercase << static_cast<uint16_t>(symbol);
        ss << ", found ";
        in_.unget();
        ss << static_cast<uint16_t>(in_.peek());
        throw std::runtime_error(ss.str());
    }
}

uint8_t JpegDecoder::ReadByte() {
    uint8_t symbol = in_.get();
    if (in_.eof()) {
        throw EofException();
    }
    if (kDebug) {
        std::printf("[%.2x]\n", symbol);
    }
    return symbol;
}
