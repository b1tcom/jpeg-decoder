#pragma once

#include <queue>
#include <cstdint>
#include <string>
#include <optional>

class HuffmanTree {
public:
    HuffmanTree();

    HuffmanTree(HuffmanTree&&);

    HuffmanTree(const HuffmanTree&);

    HuffmanTree& operator=(HuffmanTree&&);

    HuffmanTree& operator=(const HuffmanTree&);

    void BuildTree(const std::vector<uint8_t>& frequencies, const std::vector<uint8_t>& values);

    void printAllLeaves() const;

    void StartMoving();

    bool Move(uint8_t bit);

    uint8_t GetLeafValue() const;

    ~HuffmanTree();

private:
    struct Node {
        Node* next[2]{};
        std::optional<uint8_t> leaf_value;

        Node() = default;

        void CreateChildren() {
            for (int i = 0; i < 2; ++i) {
                next[i] = new Node();
            }
        }

        Node* Duplicate() const {
            auto node = new Node();
            for (size_t i = 0; i < 2; ++i) {
                if (next[i])
                    node->next[i] = next[i]->Duplicate();
            }
            node->leaf_value = leaf_value;
            return node;
        }

        void MakeLeaf(uint8_t value) {
            leaf_value = value;
        }

        ~Node() {
            delete next[0];
            delete next[1];
        }
    };

    Node* root_;
    Node* current_ = nullptr;

    void printAllLeaves(Node* node, std::string path) const;
};
