#include <algorithm>
#include <cstdint>
#include <vector>

#include "image.h"

uint8_t GetLowWord(uint8_t value);

uint8_t GetHighWord(uint8_t value);

int GetBit(int value, int pos);

int InvertFirstBits(int value, int num_of_bits);

RGB YCbCrToRgb(int Y, int Cb, int Cr, bool grayscale);
