#pragma once

#include <exception>

class EofException : public std::exception {};

class UnknownSectionMarker : public std::exception {};