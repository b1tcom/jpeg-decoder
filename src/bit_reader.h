#pragma once

#include <cstdint>
#include <istream>

class BitReader {
public:
    BitReader(std::istream& in);

    BitReader(const BitReader& reader) = delete;
    BitReader& operator=(const BitReader& reader) = delete;

    uint8_t Get();

    uint8_t ReadByte();

    uint8_t Peek() const;

private:
    constexpr static int8_t kLastPos = 8;

    std::istream& in_;
    uint8_t bite_;
    int8_t pos_ = 0;
};
