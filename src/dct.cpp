#include "dct.h"
#include <cmath>

#include "fftw3.h"

void ProcessInverseDiscreteCosineTransform(std::vector<std::vector<int>>& matrix) {
    size_t num_of_rows = matrix.size();

    std::vector<double> flat(num_of_rows * num_of_rows);
    for (size_t i = 0; i < num_of_rows; ++i) {
        for (size_t j = 0; j < num_of_rows; ++j) {
            flat[num_of_rows * i + j] = matrix[i][j];
        }
    }

    constexpr static double kSqrtTwo = sqrt(2);
    for (size_t i = 0; i < num_of_rows; ++i) {
        flat[8 * i] *= sqrt(2);
        flat[i] *= sqrt(2);
    }
    for (auto& i : flat) {
        i /= 16;
    }
    fftw_plan p = fftw_plan_r2r_2d(num_of_rows, num_of_rows,
                                   flat.data(), flat.data(),
                                   FFTW_REDFT01, FFTW_REDFT01,
                                   FFTW_ESTIMATE);
    fftw_execute(p);
    fftw_destroy_plan(p);
    for (size_t i = 0; i < num_of_rows; ++i) {
        for (size_t j = 0; j < num_of_rows; ++j) {
            matrix[i][j] = flat[num_of_rows * i + j];
        }
    }
}
