#include "bit_reader.h"
#include "exceptions.h"
#include "utility.h"

//const bool kIsDebug = true;
const bool kIsDebug = false;

BitReader::BitReader(std::istream& in)
    : in_(in) {}

uint8_t BitReader::Get() {
    if (!pos_--) {
        bite_ = ReadByte();
        if (kIsDebug) {
            std::printf("BitReader%.2X\n", int(bite_));
        }
        if (in_.eof())
            throw EofException();
        pos_ = kLastPos - 1;
    }
    return Peek();
}

uint8_t BitReader::ReadByte() {
    uint8_t bite = in_.get();
    if (bite == 0xff) {
        //next must be 0x00
        bite = in_.get();
        if (bite != 0x00 && bite != 0xff) {
            throw std::runtime_error("Expected 0x00 after 0xff");
        }
        bite = 0xff;
    }
    return bite;
}

uint8_t BitReader::Peek() const {
    return GetBit(bite_, pos_);
}
