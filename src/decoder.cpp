#include "decoder.h"

#include <fstream>
#include <ios>

#include "jpeg_decoder.h"

Image Decode(const std::string& filename) {
    std::ifstream fin(filename, std::ios_base::binary);
    JpegDecoder decoder(fin);
    decoder.Decode();
	fin.close();
    return decoder.GetImage();
}
