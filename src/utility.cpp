#include "utility.h"

uint8_t GetLowWord(uint8_t value) {
    return value & 0b1111;
}

uint8_t GetHighWord(uint8_t value) {
    return value >> 4;
}

int GetBit(int value, int pos) {
    return value >> pos & 1;
}

int InvertFirstBits(int value, int num_of_bits) {
    return value ^ ((1 << num_of_bits) - 1);
}

RGB YCbCrToRgb(int Y, int Cb, int Cr, bool grayscale = false) {
    int r = Y + 1.402 * Cr + 128;
    int g = Y - 0.34414 * Cb - 0.71414 * Cr + 128;
    int b = Y + 1.772 * Cb + 128;
    if (grayscale)
        g = b = r;
    return {
        std::clamp(r, 0, 255),
        std::clamp(g, 0, 255),
        std::clamp(b, 0, 255)};
}
