﻿#pragma once

#include <functional>
#include <map>
#include <string>
#include <unordered_map>
#include <vector>

#include "image.h"
#include "bit_reader.h"
#include "huffman.h"
#include "utility.h"


class JpegDecoder {
public:
    const uint8_t kMarker = 0xff;
    const uint8_t kSoiMarker = 0xd8;
    const uint8_t kEoiMarker = 0xd9;
    const uint8_t kComMarker = 0xfe;
    const uint8_t kApp0Marker = 0xe0;
    const uint8_t kApp1Marker = 0xe1;
    const uint8_t kDqtMarker = 0xdb;
    const uint8_t kSof0Marker = 0xc0;
    const uint8_t kDhtMarker = 0xc4;
    const uint8_t kSosMarker = 0xda;

    explicit JpegDecoder(std::istream& in);

    void Decode();

    Image GetImage() const {
        return image_;
    }

    bool IsNextSectionMarker();

    ~JpegDecoder() = default;

private:
    struct SamplingDescription {
        uint8_t component_id;
        uint8_t table_id;
        uint8_t vertical_sampling;
        uint8_t horizontal_sampling;
    };

    struct HuffmanTable {
        enum TableType { AC,
                         DC };

        uint8_t id;
        TableType type;
        std::vector<uint8_t> num_of_symbols;
        std::vector<uint8_t> symbols;
        HuffmanTree tree;
    };

    struct ComponentInfo {
        uint8_t ac_table, dc_table;
    };

    using CanonTable = std::vector<std::vector<int>>;
    using McuGrid = std::vector<std::vector<CanonTable>>;

    template <class T>
    using Grid = std::vector<std::vector<T>>;

    struct Mcu {
        std::vector<McuGrid> tables;

        static int width, height;
        static int rows, columns;
        static uint8_t max_horiz_sampling;
        static uint8_t max_vert_sampling;

        Mcu() = default;
        ~Mcu() = default;

        explicit Mcu(uint8_t num_of_components)
            : tables(num_of_components) {}
    };

    std::istream& in_;
    BitReader bit_reader_;
    std::string comment_;
    bool end_reached_ = false;
    int prev_dc_[5]{0};
    uint8_t num_of_components_;

    uint16_t image_height_, image_width_;
    Image image_;

    std::unordered_map<uint8_t, SamplingDescription> sampling_descriptions_;

    using HuffmanTableKey = std::pair<uint8_t, HuffmanTable::TableType>;
    std::map<HuffmanTableKey, HuffmanTable> huffman_tables_;

    std::unordered_map<uint8_t, ComponentInfo> components_info_;

    using Table = std::vector<std::vector<uint8_t>>;
    std::unordered_map<uint8_t, Table> quantization_tables;

    using SectionDecoder = std::function<void(JpegDecoder&)>;
    std::unordered_map<uint8_t, SectionDecoder> kSectionDecoders = {
        {JpegDecoder::kSoiMarker, &JpegDecoder::SoiDecoder},
        {JpegDecoder::kComMarker, &JpegDecoder::ComDecoder},
        {JpegDecoder::kApp0Marker, &JpegDecoder::App0Decoder},
        {JpegDecoder::kApp1Marker, &JpegDecoder::App1Decoder},
        {JpegDecoder::kDqtMarker, &JpegDecoder::DqtDecoder},
        {JpegDecoder::kSof0Marker, &JpegDecoder::Sof0Decoder},
        {JpegDecoder::kDhtMarker, &JpegDecoder::DhtDecoder},
        {JpegDecoder::kSosMarker, &JpegDecoder::SosDecoder},
        {JpegDecoder::kEoiMarker, &JpegDecoder::EoiDecoder}};

    void DispatchSection();

    void SkipSection();

    void SoiDecoder();

    void EoiDecoder();

    void ComDecoder();

    void App0Decoder();

    void App1Decoder();

    void DqtDecoder();

    template <class T>
    std::vector<std::vector<T>> OrderZigzagTable(std::vector<T>);

    void Sof0Decoder();

    SamplingDescription ReadSamplingDescription();

    void DhtDecoder();

    void SosDecoder();

    void DecodeImage(Grid<Mcu>);

    void ProcessMcu(Mcu&, size_t, size_t);

	int GetComponentValue(Mcu& mcu, size_t component, size_t mcu_y, size_t mcu_x);

    JpegDecoder::Mcu DecodeMcu();

    void QuantizeTable(CanonTable& table, const Table& quant_table);

    McuGrid ReadComponentMcuGrid(uint8_t component_id);

    std::vector<int> ReadComponentTable(uint8_t component_id);

    int ReadDc(uint8_t component_id);

    std::pair<uint8_t, int> ReadAc(uint8_t component_id);

    int ReadBitsAsNumber(uint8_t num_of_bits);

    uint16_t ReadSectionLength();

    void Match(uint8_t);

    uint8_t ReadByte();
};
